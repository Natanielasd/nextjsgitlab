import Head from 'next/head'
import Logo from '../components/Logo'

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Next.js example for GitLab Pages</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1 className="title">
          Welcome to <a href="https://nextjs.org" target="_blank" rel="noopener noreferrer"><Logo size={100} /></a>
        </h1>

        <p>Next.js example for GitLab Pages</p>
      </main>

      <style jsx>{`
        .container {
          display: flex;
          flex-direction: column;
          min-height: 100vh;
          justify-content: center;
          text-align: center;
          padding: 0 1rem;
        }
        .title {
          font-size: 5rem;
          font-weight: normal;
          margin-top: 0;
        }

        .title a {
          padding: 0 0.025em;
          border-bottom: 2px solid #1983ff;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: Arial, sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  )
}
